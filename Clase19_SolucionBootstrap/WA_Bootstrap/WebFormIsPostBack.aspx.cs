﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WA_Bootstrap
{
    public partial class WebFormIsPostBack : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ddlPais.Items.Add(new ListItem("-- Seleccione --"));
                ddlPais.Items.Add(new ListItem("Colombia"));
                ddlPais.Items.Add(new ListItem("Ecuador"));
                ddlPais.Items.Add(new ListItem("Perú"));
            }
        }

        protected void btnHacerPeticion_Click(object sender, EventArgs e)
        {
            lblInformacion.Text = ddlPais.SelectedValue;
        }

        protected void ddlPais_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlDepartamento.Items.Clear();
            ddlDepartamento.Items.Add("-- Seleccione --");

            switch (ddlPais.SelectedValue)
            {
                case "Colombia":
                    ddlDepartamento.Items.Add("Antioquia");
                    ddlDepartamento.Items.Add("Valle del Cauca");
                    break;
                case "Ecuador":
                    ddlDepartamento.Items.Add("Chaco");
                    break;
                case "Perú":
                    ddlDepartamento.Items.Add("Cusco");
                    break;
                default:
                    ddlDepartamento.Items.Add("");
                    break;
            }
        }

        protected void ddlDepartamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlCiudad.Items.Clear();

            switch (ddlDepartamento.SelectedValue)
            {
                case "Antioquia":
                    ddlCiudad.Items.Add("Medellín");
                    ddlCiudad.Items.Add("Itagüí");
                    break;
                case "Valle del Cauca":
                    ddlCiudad.Items.Add("Cali");
                    ddlCiudad.Items.Add("Tuluá");
                    break;
                default:
                    break;
            }
        }
    }
}