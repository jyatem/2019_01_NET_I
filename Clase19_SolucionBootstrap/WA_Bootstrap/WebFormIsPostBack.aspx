﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Maestra.Master" AutoEventWireup="true" CodeBehind="WebFormIsPostBack.aspx.cs" Inherits="WA_Bootstrap.WebFormIsPostBack" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="mt-3">
        <div class="card">
            <div class="card-header text-white bg-dark">
                IsPostBack
            </div>
            <div class="card-body">
                <div class="form-group row">
                    <label class="col-md-2 col-form-label">País:</label>
                    <div class="col-md-10">
                        <asp:DropDownList runat="server" ID="ddlPais" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlPais_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-2 col-form-label">Departamento:</label>
                    <div class="col-md-10">
                        <asp:DropDownList runat="server" ID="ddlDepartamento" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlDepartamento_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-2 col-form-label">Ciudad:</label>
                    <div class="col-md-10">
                        <asp:DropDownList runat="server" ID="ddlCiudad" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="offset-md-2 col-md-10">
                        <asp:Button ID="btnHacerPeticion" runat="server" Text="Hacer petición" CssClass="btn btn-dark" OnClick="btnHacerPeticion_Click"/>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="offset-md-2 col-md-10">
                        <asp:Label ID="lblInformacion" runat="server" Text="" CssClass="text-success"></asp:Label>
                    </div>
                </div>

            </div>
        </div>
    </div>

</asp:Content>
