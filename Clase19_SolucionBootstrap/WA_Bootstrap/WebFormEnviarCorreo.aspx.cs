﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WA_Bootstrap
{
    public partial class WebFormEnviarCorreo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            try
            {
                SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", 587);

                smtpClient.UseDefaultCredentials = true;
                smtpClient.Credentials = new NetworkCredential("pruebascedesistemasnetbasico@gmail.com", "Cedesistemas2019*");
                smtpClient.EnableSsl = true;                

                MailMessage mailMessage = new MailMessage();
                mailMessage.Subject = txtAsunto.Text;
                mailMessage.To.Add(new MailAddress(txtPara.Text));
                mailMessage.From = new MailAddress("pruebascedesistemasnetbasico@gmail.com", "Yate");
                mailMessage.IsBodyHtml = true;

                mailMessage.Body = @"<h1 style='color:red'>" + txtMensaje.Text + "</h1>";

                string archivo = Server.MapPath("~/Archivos/NuGet.pptx");

                Attachment adjunto = new Attachment(archivo);

                mailMessage.Attachments.Add(adjunto);

                smtpClient.Send(mailMessage);

                lblInformacion.CssClass = "text-success";
                lblInformacion.Text = "Correo enviado exitosamente";
            }
            catch (Exception ex)
            {
                lblInformacion.CssClass = "text-danger";
                lblInformacion.Text = ex.Message;
            }
        }
    }
}