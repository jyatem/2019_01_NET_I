﻿using LogicaNegocio;
using Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Referencias
{
    class Program
    {
        static void Main(string[] args)
        {
            LogicaCurso logicaCurso = new LogicaCurso("C001", ".NET I", 110);

            logicaCurso.AgregarEstudiante(new Estudiante { Id = 1, Nombre = "Jairo", Apellidos = "Yate" });
            logicaCurso.AgregarEstudiante(new Estudiante { Id = 2, Nombre = "Lina", Apellidos = "Mora" });

            foreach (Estudiante estudiante in logicaCurso.MiCurso.Estudiantes)
            {
                Console.WriteLine($"Estudiante: {estudiante.Nombre} {estudiante.Apellidos}");
            }

            Console.ReadKey();
        }
    }
}
