﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MVVMXam.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        private string _miMensaje;

        public MainViewModel()
        {
            MiMensaje = "¡Hola MVVM!";
        }

        public string MiMensaje
        {
            get { return _miMensaje; }
            set
            {
                _miMensaje = value;
                OnPropertyChanged();
            }
        }
    }
}
