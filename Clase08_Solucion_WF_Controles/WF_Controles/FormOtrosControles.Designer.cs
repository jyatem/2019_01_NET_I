﻿namespace WF_Controles
{
    partial class FormOtrosControles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormOtrosControles));
            this.btnIzquierdo = new System.Windows.Forms.Button();
            this.btnCentro = new System.Windows.Forms.Button();
            this.btnDerecho = new System.Windows.Forms.Button();
            this.btnSuperior = new System.Windows.Forms.Button();
            this.pbBarraProgreso = new System.Windows.Forms.ProgressBar();
            this.btnEjecutar = new System.Windows.Forms.Button();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.lblEdad = new System.Windows.Forms.Label();
            this.txtEdad = new System.Windows.Forms.TextBox();
            this.btnValidar = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnIzquierdo
            // 
            this.btnIzquierdo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnIzquierdo.Location = new System.Drawing.Point(29, 395);
            this.btnIzquierdo.Name = "btnIzquierdo";
            this.btnIzquierdo.Size = new System.Drawing.Size(75, 23);
            this.btnIzquierdo.TabIndex = 0;
            this.btnIzquierdo.Text = "button1";
            this.btnIzquierdo.UseVisualStyleBackColor = true;
            // 
            // btnCentro
            // 
            this.btnCentro.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnCentro.Location = new System.Drawing.Point(187, 395);
            this.btnCentro.Name = "btnCentro";
            this.btnCentro.Size = new System.Drawing.Size(75, 23);
            this.btnCentro.TabIndex = 1;
            this.btnCentro.Text = "button1";
            this.btnCentro.UseVisualStyleBackColor = true;
            // 
            // btnDerecho
            // 
            this.btnDerecho.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDerecho.Location = new System.Drawing.Point(344, 395);
            this.btnDerecho.Name = "btnDerecho";
            this.btnDerecho.Size = new System.Drawing.Size(75, 23);
            this.btnDerecho.TabIndex = 2;
            this.btnDerecho.Text = "button2";
            this.btnDerecho.UseVisualStyleBackColor = true;
            // 
            // btnSuperior
            // 
            this.btnSuperior.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnSuperior.Location = new System.Drawing.Point(0, 0);
            this.btnSuperior.Name = "btnSuperior";
            this.btnSuperior.Size = new System.Drawing.Size(452, 23);
            this.btnSuperior.TabIndex = 3;
            this.btnSuperior.Text = "button1";
            this.btnSuperior.UseVisualStyleBackColor = true;
            // 
            // pbBarraProgreso
            // 
            this.pbBarraProgreso.Location = new System.Drawing.Point(41, 53);
            this.pbBarraProgreso.Name = "pbBarraProgreso";
            this.pbBarraProgreso.Size = new System.Drawing.Size(391, 23);
            this.pbBarraProgreso.TabIndex = 4;
            // 
            // btnEjecutar
            // 
            this.btnEjecutar.Location = new System.Drawing.Point(193, 93);
            this.btnEjecutar.Name = "btnEjecutar";
            this.btnEjecutar.Size = new System.Drawing.Size(75, 23);
            this.btnEjecutar.TabIndex = 5;
            this.btnEjecutar.Text = "Ejecutar";
            this.btnEjecutar.UseVisualStyleBackColor = true;
            this.btnEjecutar.Click += new System.EventHandler(this.btnEjecutar_Click);
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // lblEdad
            // 
            this.lblEdad.AutoSize = true;
            this.lblEdad.Location = new System.Drawing.Point(210, 138);
            this.lblEdad.Name = "lblEdad";
            this.lblEdad.Size = new System.Drawing.Size(32, 13);
            this.lblEdad.TabIndex = 6;
            this.lblEdad.Text = "Edad";
            // 
            // txtEdad
            // 
            this.txtEdad.Location = new System.Drawing.Point(178, 154);
            this.txtEdad.Name = "txtEdad";
            this.txtEdad.Size = new System.Drawing.Size(100, 20);
            this.txtEdad.TabIndex = 7;
            // 
            // btnValidar
            // 
            this.btnValidar.Location = new System.Drawing.Point(193, 182);
            this.btnValidar.Name = "btnValidar";
            this.btnValidar.Size = new System.Drawing.Size(75, 23);
            this.btnValidar.TabIndex = 8;
            this.btnValidar.Text = "Validar";
            this.btnValidar.UseVisualStyleBackColor = true;
            this.btnValidar.Click += new System.EventHandler(this.btnValidar_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(169, 230);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(123, 118);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // FormOtrosControles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(452, 435);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnValidar);
            this.Controls.Add(this.txtEdad);
            this.Controls.Add(this.lblEdad);
            this.Controls.Add(this.btnEjecutar);
            this.Controls.Add(this.pbBarraProgreso);
            this.Controls.Add(this.btnSuperior);
            this.Controls.Add(this.btnDerecho);
            this.Controls.Add(this.btnCentro);
            this.Controls.Add(this.btnIzquierdo);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormOtrosControles";
            this.Text = "FormOtrosControles";
            this.Load += new System.EventHandler(this.FormOtrosControles_Load);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnIzquierdo;
        private System.Windows.Forms.Button btnCentro;
        private System.Windows.Forms.Button btnDerecho;
        private System.Windows.Forms.Button btnSuperior;
        private System.Windows.Forms.ProgressBar pbBarraProgreso;
        private System.Windows.Forms.Button btnEjecutar;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ErrorProvider errorProvider;
        private System.Windows.Forms.Button btnValidar;
        private System.Windows.Forms.TextBox txtEdad;
        private System.Windows.Forms.Label lblEdad;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}