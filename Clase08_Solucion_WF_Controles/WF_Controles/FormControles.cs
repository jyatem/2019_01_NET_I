﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_Controles
{
    public partial class FormControles : Form
    {
        public FormControles()
        {
            InitializeComponent();
        }

        private void btnObtenerInformacion_Click(object sender, EventArgs e)
        {
            try
            {
                lblNombre.Text = "Nombre cambiado";

                string nombre = txtNombre.Text;
                string pais = cmbPaises.Text;
                string hobbies = "";
                string lenguajes = "";
                string genero = rdoFemenino.Checked ? "Femenino" : "Masculino";
                string estadoCivil = rdoCasado.Checked ? "Casado" : "Soltero";
                string estado = chkActivo.Checked ? "Está activo" : "Esta inactivo";
                DateTime fecha = dtpFecha.Value;

                foreach (var item in lstHobbies.SelectedItems)
                {
                    //hobbies = hobbies + item.ToString() + ",";
                    hobbies += item.ToString() + ", ";
                }

                foreach (var item in chklstLenguajes.CheckedItems)
                {
                    lenguajes += item.ToString() + ", ";
                }

                MessageBox.Show(String.Format("Nombre: {0}{1}País: {2}{1}Hobbies: {3}{1}Lenguajes: {4}{1}Genero: {5}{1}Estado Civil: {6}{1}Estado: {7}{1}Fecha: {8:yyyy-MM-dd}", nombre, Environment.NewLine, pais, hobbies, lenguajes, genero, estadoCivil, estado, fecha));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mtxtFecha_TypeValidationCompleted(object sender, TypeValidationEventArgs fechaEvaluar)
        {
            if (!fechaEvaluar.IsValidInput)
            {
                //MessageBox.Show("La fecha no es válida");
                //mtxtFecha.Focus();
            }
        }
    }
}
