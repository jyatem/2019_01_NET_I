﻿namespace WF_Controles
{
    partial class FormControles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormControles));
            this.lblNombre = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.btnObtenerInformacion = new System.Windows.Forms.Button();
            this.mtxtFecha = new System.Windows.Forms.MaskedTextBox();
            this.rtxtContenido = new System.Windows.Forms.RichTextBox();
            this.cmbPaises = new System.Windows.Forms.ComboBox();
            this.lstHobbies = new System.Windows.Forms.ListBox();
            this.chklstLenguajes = new System.Windows.Forms.CheckedListBox();
            this.rdoFemenino = new System.Windows.Forms.RadioButton();
            this.rdoMasculino = new System.Windows.Forms.RadioButton();
            this.gboxEstadoCivil = new System.Windows.Forms.GroupBox();
            this.rdoCasado = new System.Windows.Forms.RadioButton();
            this.rdoSoltero = new System.Windows.Forms.RadioButton();
            this.chkActivo = new System.Windows.Forms.CheckBox();
            this.dtpFecha = new System.Windows.Forms.DateTimePicker();
            this.gboxEstadoCivil.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Location = new System.Drawing.Point(32, 22);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(47, 13);
            this.lblNombre.TabIndex = 0;
            this.lblNombre.Text = "Nombre:";
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(32, 43);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(256, 20);
            this.txtNombre.TabIndex = 1;
            // 
            // btnObtenerInformacion
            // 
            this.btnObtenerInformacion.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnObtenerInformacion.Location = new System.Drawing.Point(82, 470);
            this.btnObtenerInformacion.Name = "btnObtenerInformacion";
            this.btnObtenerInformacion.Size = new System.Drawing.Size(156, 23);
            this.btnObtenerInformacion.TabIndex = 4;
            this.btnObtenerInformacion.Text = "Obtener Información";
            this.btnObtenerInformacion.UseVisualStyleBackColor = true;
            this.btnObtenerInformacion.Click += new System.EventHandler(this.btnObtenerInformacion_Click);
            // 
            // mtxtFecha
            // 
            this.mtxtFecha.Location = new System.Drawing.Point(32, 115);
            this.mtxtFecha.Mask = "00/00/0000";
            this.mtxtFecha.Name = "mtxtFecha";
            this.mtxtFecha.Size = new System.Drawing.Size(123, 20);
            this.mtxtFecha.TabIndex = 3;
            this.mtxtFecha.ValidatingType = typeof(System.DateTime);
            this.mtxtFecha.TypeValidationCompleted += new System.Windows.Forms.TypeValidationEventHandler(this.mtxtFecha_TypeValidationCompleted);
            // 
            // rtxtContenido
            // 
            this.rtxtContenido.Location = new System.Drawing.Point(32, 71);
            this.rtxtContenido.Name = "rtxtContenido";
            this.rtxtContenido.Size = new System.Drawing.Size(256, 36);
            this.rtxtContenido.TabIndex = 2;
            this.rtxtContenido.Text = "";
            // 
            // cmbPaises
            // 
            this.cmbPaises.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPaises.FormattingEnabled = true;
            this.cmbPaises.Items.AddRange(new object[] {
            "Colombia",
            "Argentina",
            "Ecuador",
            "Perú"});
            this.cmbPaises.Location = new System.Drawing.Point(32, 143);
            this.cmbPaises.Name = "cmbPaises";
            this.cmbPaises.Size = new System.Drawing.Size(194, 21);
            this.cmbPaises.TabIndex = 5;
            // 
            // lstHobbies
            // 
            this.lstHobbies.FormattingEnabled = true;
            this.lstHobbies.Items.AddRange(new object[] {
            "Musica",
            "Cine",
            "Caminata",
            "Xbox"});
            this.lstHobbies.Location = new System.Drawing.Point(32, 172);
            this.lstHobbies.Name = "lstHobbies";
            this.lstHobbies.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lstHobbies.Size = new System.Drawing.Size(120, 56);
            this.lstHobbies.TabIndex = 6;
            // 
            // chklstLenguajes
            // 
            this.chklstLenguajes.FormattingEnabled = true;
            this.chklstLenguajes.Items.AddRange(new object[] {
            "C#",
            "Visual Basic",
            "PHP",
            "HTML",
            "JavaScript"});
            this.chklstLenguajes.Location = new System.Drawing.Point(32, 236);
            this.chklstLenguajes.Name = "chklstLenguajes";
            this.chklstLenguajes.Size = new System.Drawing.Size(120, 49);
            this.chklstLenguajes.TabIndex = 7;
            // 
            // rdoFemenino
            // 
            this.rdoFemenino.AutoSize = true;
            this.rdoFemenino.Checked = true;
            this.rdoFemenino.Location = new System.Drawing.Point(32, 293);
            this.rdoFemenino.Name = "rdoFemenino";
            this.rdoFemenino.Size = new System.Drawing.Size(71, 17);
            this.rdoFemenino.TabIndex = 8;
            this.rdoFemenino.TabStop = true;
            this.rdoFemenino.Text = "Femenino";
            this.rdoFemenino.UseVisualStyleBackColor = true;
            // 
            // rdoMasculino
            // 
            this.rdoMasculino.AutoSize = true;
            this.rdoMasculino.Location = new System.Drawing.Point(124, 293);
            this.rdoMasculino.Name = "rdoMasculino";
            this.rdoMasculino.Size = new System.Drawing.Size(73, 17);
            this.rdoMasculino.TabIndex = 9;
            this.rdoMasculino.Text = "Masculino";
            this.rdoMasculino.UseVisualStyleBackColor = true;
            // 
            // gboxEstadoCivil
            // 
            this.gboxEstadoCivil.Controls.Add(this.rdoSoltero);
            this.gboxEstadoCivil.Controls.Add(this.rdoCasado);
            this.gboxEstadoCivil.Location = new System.Drawing.Point(35, 317);
            this.gboxEstadoCivil.Name = "gboxEstadoCivil";
            this.gboxEstadoCivil.Size = new System.Drawing.Size(200, 59);
            this.gboxEstadoCivil.TabIndex = 10;
            this.gboxEstadoCivil.TabStop = false;
            this.gboxEstadoCivil.Text = "Estado Civil";
            // 
            // rdoCasado
            // 
            this.rdoCasado.AutoSize = true;
            this.rdoCasado.Location = new System.Drawing.Point(10, 27);
            this.rdoCasado.Name = "rdoCasado";
            this.rdoCasado.Size = new System.Drawing.Size(61, 17);
            this.rdoCasado.TabIndex = 0;
            this.rdoCasado.Text = "Casado";
            this.rdoCasado.UseVisualStyleBackColor = true;
            // 
            // rdoSoltero
            // 
            this.rdoSoltero.AutoSize = true;
            this.rdoSoltero.Checked = true;
            this.rdoSoltero.Location = new System.Drawing.Point(101, 27);
            this.rdoSoltero.Name = "rdoSoltero";
            this.rdoSoltero.Size = new System.Drawing.Size(58, 17);
            this.rdoSoltero.TabIndex = 1;
            this.rdoSoltero.TabStop = true;
            this.rdoSoltero.Text = "Soltero";
            this.rdoSoltero.UseVisualStyleBackColor = true;
            // 
            // chkActivo
            // 
            this.chkActivo.AutoSize = true;
            this.chkActivo.Location = new System.Drawing.Point(34, 384);
            this.chkActivo.Name = "chkActivo";
            this.chkActivo.Size = new System.Drawing.Size(56, 17);
            this.chkActivo.TabIndex = 11;
            this.chkActivo.Text = "Activo";
            this.chkActivo.UseVisualStyleBackColor = true;
            // 
            // dtpFecha
            // 
            this.dtpFecha.Location = new System.Drawing.Point(32, 408);
            this.dtpFecha.Name = "dtpFecha";
            this.dtpFecha.Size = new System.Drawing.Size(200, 20);
            this.dtpFecha.TabIndex = 12;
            // 
            // FormControles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(321, 516);
            this.Controls.Add(this.dtpFecha);
            this.Controls.Add(this.chkActivo);
            this.Controls.Add(this.gboxEstadoCivil);
            this.Controls.Add(this.rdoMasculino);
            this.Controls.Add(this.rdoFemenino);
            this.Controls.Add(this.chklstLenguajes);
            this.Controls.Add(this.lstHobbies);
            this.Controls.Add(this.cmbPaises);
            this.Controls.Add(this.rtxtContenido);
            this.Controls.Add(this.mtxtFecha);
            this.Controls.Add(this.btnObtenerInformacion);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.lblNombre);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.Name = "FormControles";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Controles";
            this.gboxEstadoCivil.ResumeLayout(false);
            this.gboxEstadoCivil.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Button btnObtenerInformacion;
        private System.Windows.Forms.MaskedTextBox mtxtFecha;
        private System.Windows.Forms.RichTextBox rtxtContenido;
        private System.Windows.Forms.ComboBox cmbPaises;
        private System.Windows.Forms.ListBox lstHobbies;
        private System.Windows.Forms.CheckedListBox chklstLenguajes;
        private System.Windows.Forms.RadioButton rdoFemenino;
        private System.Windows.Forms.RadioButton rdoMasculino;
        private System.Windows.Forms.GroupBox gboxEstadoCivil;
        private System.Windows.Forms.RadioButton rdoSoltero;
        private System.Windows.Forms.RadioButton rdoCasado;
        private System.Windows.Forms.CheckBox chkActivo;
        private System.Windows.Forms.DateTimePicker dtpFecha;
    }
}

