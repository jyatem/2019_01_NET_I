﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebFormSumar.aspx.cs" Inherits="ClienteServicioWebCursos.WebFormSumar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Label ID="lblValor1" runat="server" Text="Valor 1:"></asp:Label>
        <asp:TextBox ID="txtValor1" runat="server"></asp:TextBox>
        <asp:Label ID="lblValor2" runat="server" Text="Valor 2:"></asp:Label>
        <asp:TextBox ID="txtValor2" runat="server"></asp:TextBox>
        <asp:Button ID="btnSumar" runat="server" Text="Sumar" OnClick="btnSumar_Click"/>
        <asp:Label ID="lblResultado" runat="server" Text=""></asp:Label>
    </form>
</body>
</html>
