﻿using ClienteServicioWebCursos.ReferenciaServicioWebCursos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ClienteServicioWebCursos
{
    public partial class WebFormCiudades : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SWCursos sWCursos = new SWCursos();
            gvCiudades.DataSource = sWCursos.RetornarCiudades();
            gvCiudades.DataBind();
        }
    }
}