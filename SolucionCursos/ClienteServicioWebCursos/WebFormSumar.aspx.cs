﻿using ClienteServicioWebCursos.ReferenciaServicioWebCursos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ClienteServicioWebCursos
{
    public partial class WebFormSumar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSumar_Click(object sender, EventArgs e)
        {
            try
            {
                SWCursos sWCursos = new SWCursos();
                lblResultado.Text = sWCursos.Sumar(Convert.ToInt32(txtValor1.Text), Convert.ToInt32(txtValor2.Text)).ToString();
            }
            catch (Exception ex)
            {
                lblResultado.Text = ex.Message;
            }
        }
    }
}