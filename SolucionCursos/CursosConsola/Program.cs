﻿using AccesoDatos;
using LogicaNegocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursosConsola
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                FachadaEstudiante fachadaEstudiante = new FachadaEstudiante();

                #region [ Insertar Estudiante ]
                //Estudiante estudiante = new Estudiante
                //{
                //    Cedula = 90,
                //    Nombre = "Jairo",
                //    Apellidos = "Yate",
                //    CiudadNacimiento = 10,
                //    Correo = "jyatem@yahoo.com",
                //    Genero = true
                //};

                //int cantidad = fachadaEstudiante.InsertarEstudiante(estudiante);

                //if (cantidad == 1)
                //{
                //    Console.WriteLine("Estudiante ingresado exitosamente");
                //}
                //else
                //{
                //    Console.WriteLine("Estudiante no se pudo ingresar");
                //} 
                #endregion

                #region [ Obtener un Estudiante ]
                //Estudiante estudiante = fachadaEstudiante.RetornarEstudiante(80);

                //if (estudiante != null)
                //{
                //    Console.WriteLine($"{estudiante.Nombre} {estudiante.Apellidos}");
                //}
                //else
                //{
                //    Console.WriteLine("Estudiante no existe");
                //}
                #endregion

                #region [ Obtener Estudiantes ]
                foreach (Estudiante estudiante in fachadaEstudiante.RetornarEstudiantes())
                {
                    Console.WriteLine($"{estudiante.Nombre} {estudiante.Apellidos}");
                }
                #endregion
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.ReadLine();
            }
        }
    }
}
