﻿using AccesoDatos;
using AccesoDatos.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicaNegocio
{
    public class FachadaCiudad
    {
        private DBCursosEntities _contexto;

        public FachadaCiudad()
        {
            _contexto = new DBCursosEntities();
        }

        public List<Ciudad> RetornarCiudades()
        {
            return _contexto.Ciudads.OrderBy(c => c.NombreCiudad).ToList();
        }

        public List<CiudadDTO> RetornarCiudadesDTO()
        {
            return _contexto.Ciudads.OrderBy(c => c.NombreCiudad).Select(c => new CiudadDTO { Id = c.Id, NombreCiudad = c.NombreCiudad }).ToList();
        }
    }
}
