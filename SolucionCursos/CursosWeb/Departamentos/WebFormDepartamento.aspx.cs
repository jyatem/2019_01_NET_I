﻿using AccesoDatos;
using LogicaNegocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CursosWeb.Departamentos
{
    public partial class WebFormDepartamento : System.Web.UI.Page
    {
        private FachadaDepartamento _fachadaDepartamento = new FachadaDepartamento();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!String.IsNullOrEmpty(Request["Id"]))
                {
                    CargarDepartamento(Convert.ToInt32(Request["Id"]));
                }
            }
        }

        private void CargarDepartamento(int id)
        {
            try
            {
                Departamento departamento = _fachadaDepartamento.RetornarDepartamento(id);

                if (departamento != null)
                {
                    txtId.Text = id.ToString();

                    txtNombre.Text = departamento.NombreDepartamento;

                    btnGuardar.Text = "Actualizar departamento";
                }
                else
                {
                    lblInformacion.CssClass = "text-danger";
                    lblInformacion.Text = $"El departamento con el id {id} no existe";
                }
            }
            catch (Exception ex)
            {
                lblInformacion.CssClass = "text-danger";
                lblInformacion.Text = ex.Message;
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    Departamento departamento = new Departamento
                    {
                        NombreDepartamento = txtNombre.Text
                    };

                    if (btnGuardar.Text == "Actualizar departamento")
                    {
                        departamento.Id = Convert.ToInt32(txtId.Text);

                        if (_fachadaDepartamento.ActualizarDepartamento(departamento) == 1)
                        {
                            Response.Redirect("~/Departamentos/WebFormListarDepartamentos.aspx");
                        }
                        else
                        {
                            lblInformacion.CssClass = "text-danger";
                            lblInformacion.Text = "El departamento no se pudo actualizar";
                        }
                    }
                    else
                    {
                        if (_fachadaDepartamento.InsertarDepartamento(departamento) == 1)
                        {
                            txtNombre.Text = "";
                            
                            lblInformacion.CssClass = "text-success";
                            lblInformacion.Text = "El departamento se ingresó correctamente";
                        }
                        else
                        {
                            lblInformacion.CssClass = "text-danger";
                            lblInformacion.Text = "El departamento no se pudo ingresar";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lblInformacion.CssClass = "text-danger";
                lblInformacion.Text = ex.Message;
            }
        }
    }
}