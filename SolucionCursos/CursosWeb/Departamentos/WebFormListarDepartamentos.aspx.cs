﻿using LogicaNegocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CursosWeb.Departamentos
{
    public partial class WebFormListarDepartamentos : System.Web.UI.Page
    {
        private FachadaDepartamento _fachadaDepartamento = new FachadaDepartamento();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CargarDepartamentos();
            }
        }

        private void CargarDepartamentos()
        {
            gvDepartamentos.DataSource = _fachadaDepartamento.RetornarDepartamentos(txtNombre.Text);
            gvDepartamentos.DataBind();
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            CargarDepartamentos();
        }

        protected void gvDepartamentos_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvDepartamentos.PageIndex = e.NewPageIndex;
            CargarDepartamentos();
        }

        protected void gvDepartamentos_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Eliminar")
            {
                int id = Convert.ToInt32(e.CommandArgument);

                if (_fachadaDepartamento.EliminarDepartamento(id) == 1)
                {
                    CargarDepartamentos();
                    lblInformacion.CssClass = "text-success";
                    lblInformacion.Text = $"Departamento eliminado exitosamente ({DateTime.Now.ToString()})";
                }
                else
                {
                    lblInformacion.CssClass = "text-danger";
                    lblInformacion.Text = $"El departamento no se pudo eliminar ({DateTime.Now.ToString()})";
                }
            }
        }
    }
}