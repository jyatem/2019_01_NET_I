﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Maestra.Master" AutoEventWireup="true" CodeBehind="WebFormListarEstudiantes.aspx.cs" Inherits="CursosWeb.Estudiantes.WebFormListarEstudiantes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <link href="../Content/paginacion.css" rel="stylesheet" />

    <div class="form-inline mt-3">
        <div class="form-group">
            <asp:TextBox runat="server" ID="txtNombre" CssClass="form-control mr-2" placeholder="Nombre del estudiante"/>
            <asp:TextBox runat="server" ID="txtApellidos" CssClass="form-control mr-2" placeholder="Apellido del estudiante"/>
            <asp:DropDownList runat="server" ID="ddlCiudadNacimiento" CssClass="form-control mr-2"/>
            <asp:Button Text="Buscar" runat="server" ID="btnBuscar" CssClass="btn btn-primary" OnClick="btnBuscar_Click"/>
        </div>
    </div>

    <asp:GridView ID="gvEstudiantes" runat="server" CssClass="table table-striped table-dark table-hover mt-3" AutoGenerateColumns="false" AllowPaging="true" PageSize="10" OnPageIndexChanging="gvEstudiantes_PageIndexChanging" OnRowCommand="gvEstudiantes_RowCommand" EmptyDataText="No existen estudiantes" AllowSorting="true" OnSorting="gvEstudiantes_Sorting">
        <Columns>
            <asp:HyperLinkField DataTextField="Cedula" HeaderText="Cédula" DataNavigateUrlFormatString="~/Estudiantes/WebFormEstudiante.aspx?Cedula={0}" DataNavigateUrlFields="Cedula" />
            <asp:BoundField HeaderText="Nombre del estudiante" DataField="NombreCompleto" SortExpression="Nombre"/>
            <asp:BoundField HeaderText="Ciudad" DataField="NombreCiudadNacimiento" />
            <asp:BoundField HeaderText="Genero" DataField="Genero" />
            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="100px">
                <ItemTemplate>
                    <asp:Button Text="Eliminar" runat="server" ID="btnEliminar" CssClass="btn btn-danger" OnClientClick="return confirm('¿Realmente desea eliminar este estudiante?');" CommandName="Eliminar" CommandArgument='<%# Bind("Cedula") %>'/>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>

    <asp:Label ID="lblInformacion" runat="server" Text="" CssClass="text-success"></asp:Label>

</asp:Content>
