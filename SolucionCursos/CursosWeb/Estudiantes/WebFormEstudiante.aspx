﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Maestra.Master" AutoEventWireup="true" CodeBehind="WebFormEstudiante.aspx.cs" Inherits="CursosWeb.Estudiantes.WebFormEstudiante" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:HyperLink NavigateUrl="?lang=es" runat="server" ID="hlIngles" Text="<%$Resources:multilenguaje, lang %>" />
    <asp:HyperLink NavigateUrl="?lang=en" runat="server" ID="hlEspanol" Text="<%$Resources:multilenguaje, lang %>" />

    <div class="mt-3">
        <div class="card">
            <div class="card-header text-white bg-dark">
                Estudiante
            </div>
            <div class="card-body">
                <asp:ValidationSummary ID="vsResumen" runat="server" DisplayMode="BulletList" ShowMessageBox="false" ShowSummary="true" CssClass="alert alert-danger" />

                <!-- Cédula -->
                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="txtCedula">
                        <asp:Literal Text="<%$Resources:multilenguaje, cedula %>" runat="server" />
                    </label>
                    <div class="col-md-10">
                        <asp:TextBox ID="txtCedula" runat="server" CssClass="form-control" Placeholder="Ingrese la cédula"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvCedula" runat="server" ErrorMessage="La cédula es requerido" ControlToValidate="txtCedula" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger" />
                        <asp:CompareValidator ID="cvCedula" runat="server" ErrorMessage="La cédula debe ser un número entero" ControlToValidate="txtCedula" Operator="DataTypeCheck" Type="Integer" CssClass="alert-danger" SetFocusOnError="true" Display="Dynamic" />
                    </div>
                </div>

                <!-- Nombre -->
                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="txtNombre">
                        <asp:Literal Text="<%$Resources:multilenguaje, nombre%>" runat="server" />
                    </label>
                    <div class="col-md-10">
                        <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control" Placeholder="Ingrese el nombre" MaxLength="250"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvNombre" runat="server" ErrorMessage="El nombre es requerido" ControlToValidate="txtNombre" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger" />
                        <asp:RegularExpressionValidator ID="revNombre" runat="server" ErrorMessage="Sólo letras y espacio en blanco" ControlToValidate="txtNombre" ValidationExpression="^[ A-Za-zÑñáéíóú]+$" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger" />
                    </div>
                </div>

                <!-- Apellidos -->
                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="txtApellidos">
                        <asp:Literal Text="<%$Resources:multilenguaje, apellido%>" runat="server" />
                    </label>
                    <div class="col-md-10">
                        <asp:TextBox ID="txtApellidos" runat="server" CssClass="form-control" Placeholder="Ingrese el apellido" MaxLength="250"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="revApellidos" runat="server" ErrorMessage="Sólo letras y espacio en blanco" ControlToValidate="txtApellidos" ValidationExpression="^[ A-Za-zÑñáéíóú]+$" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger" />
                    </div>
                </div>

                <!-- Correo -->
                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="txtCorreo">Correo:</label>
                    <div class="col-md-10">
                        <asp:TextBox ID="txtCorreo" runat="server" CssClass="form-control" Placeholder="Ingrese el correo" MaxLength="100"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="revCorreo" runat="server" ErrorMessage="El correo no es válido" ControlToValidate="txtCorreo" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger" />
                    </div>
                </div>

                <!-- Genero -->
                <div class="form-group row">
                    <label class="col-md-2 col-form-label">Genero:</label>
                    <div class="col-md-10">
                        <asp:RadioButtonList ID="rdoGenero" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Text="Femenino" Value="false" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Masculino" Value="true"></asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>

                <!-- Ciudad de Nacimiento -->
                <div class="form-group row">
                    <label class="col-md-2 col-form-label">Ciudad Nacimiento:</label>
                    <div class="col-md-10">
                        <asp:DropDownList ID="ddlCiudadNacimiento" runat="server" CssClass="form-control"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvCiudadNacimiento" runat="server" ErrorMessage="La ciudad de nacimiento es requerida" ControlToValidate="ddlCiudadNacimiento" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger" />
                    </div>
                </div>

                <!-- Botón de guardar -->
                <div class="form-group row">
                    <div class="offset-md-2 col-md-10">
                        <asp:Button ID="btnGuardar" runat="server" Text="Insertar" CssClass="btn btn-dark" OnClick="btnGuardar_Click" />
                    </div>
                </div>

                <!-- Label de información -->
                <div class="form-group row">
                    <div class="offset-md-2 col-md-10">
                        <asp:Label ID="lblInformacion" runat="server" Text="" CssClass="text-success"></asp:Label>
                    </div>
                </div>

            </div>
        </div>
    </div>

</asp:Content>
