﻿using AccesoDatos;
using LogicaNegocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CursosWeb.Estudiantes
{
    public partial class WebFormEstudiante : BasePage
    {
        FachadaEstudiante _fachadaEstudiante = new FachadaEstudiante();

        #region [ Eventos de la página ]
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                #region [ Cargar ciudad ]
                FachadaCiudad fachadaCiudad = new FachadaCiudad();

                ddlCiudadNacimiento.DataSource = fachadaCiudad.RetornarCiudades();
                ddlCiudadNacimiento.DataValueField = "Id";
                ddlCiudadNacimiento.DataTextField = "NombreCiudad";
                ddlCiudadNacimiento.DataBind();

                ddlCiudadNacimiento.Items.Insert(0, new ListItem("-- Seleccione una ciudad --", ""));
                #endregion

                if (!String.IsNullOrEmpty(Request["Cedula"]))
                {
                    CargarEstudiante(Convert.ToInt32(Request["Cedula"]));
                }

                if (string.IsNullOrEmpty(Convert.ToString(Session["lang"])))
                {
                    hlIngles.Visible = false;
                    hlEspanol.Visible = true;
                }
                else
                {
                    string lang = Session["lang"].ToString();

                    if (lang.Equals("es"))
                    {
                        hlIngles.Visible = false;
                        hlEspanol.Visible = true;
                    }
                    else
                    {
                        hlIngles.Visible = true;
                        hlEspanol.Visible = false;
                    }
                }
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    Estudiante estudiante = new Estudiante
                    {
                        Cedula = Convert.ToInt32(txtCedula.Text),
                        Nombre = txtNombre.Text,
                        Apellidos = txtApellidos.Text,
                        Correo = txtCorreo.Text,
                        Genero = Convert.ToBoolean(rdoGenero.SelectedValue),
                        CiudadNacimiento = Convert.ToInt32(ddlCiudadNacimiento.SelectedValue)
                    };

                    if (!txtCedula.Enabled)
                    {
                        if (_fachadaEstudiante.ActualizarEstudiante(estudiante) == 1)
                        {
                            Response.Redirect("~/Estudiantes/WebFormListarEstudiantes.aspx");
                        }
                        else
                        {
                            lblInformacion.CssClass = "text-danger";
                            lblInformacion.Text = "El estudiante no se pudo actualizar";
                        }
                    }
                    else
                    {
                        if (_fachadaEstudiante.InsertarEstudiante(estudiante) == 1)
                        {
                            txtApellidos.Text = txtCedula.Text = txtCorreo.Text = txtNombre.Text = "";
                            rdoGenero.SelectedValue = "false";
                            ddlCiudadNacimiento.SelectedValue = "";

                            lblInformacion.CssClass = "text-success";
                            lblInformacion.Text = "El estudiante se ingresó correctamente";
                        }
                        else
                        {
                            lblInformacion.CssClass = "text-danger";
                            lblInformacion.Text = "El estudiante no se pudo ingresar";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lblInformacion.CssClass = "text-danger";
                lblInformacion.Text = ex.Message;
            }
        }
        #endregion

        #region [ Métodos privados ]
        private void CargarEstudiante(int cedula)
        {
            try
            {
                Estudiante estudiante = _fachadaEstudiante.RetornarEstudiante(cedula);

                if (estudiante != null)
                {
                    txtCedula.Text = cedula.ToString();
                    txtCedula.Enabled = false;

                    txtNombre.Text = estudiante.Nombre;
                    txtApellidos.Text = estudiante.Apellidos;
                    ddlCiudadNacimiento.SelectedValue = estudiante.CiudadNacimiento.ToString();
                    txtCorreo.Text = estudiante.Correo;
                    rdoGenero.SelectedValue = estudiante.Genero.ToString().ToLower();

                    btnGuardar.Text = "Actualizar estudiante";
                }
                else
                {
                    lblInformacion.CssClass = "text-danger";
                    lblInformacion.Text = $"El estudiante con la cédula {cedula} no existe";
                }
            }
            catch (Exception ex)
            {
                lblInformacion.CssClass = "text-danger";
                lblInformacion.Text = ex.Message;
            }
        }
        #endregion
    }
}