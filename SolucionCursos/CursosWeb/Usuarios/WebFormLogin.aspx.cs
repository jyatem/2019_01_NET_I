﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CursosWeb.Usuarios
{
    public partial class WebFormLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (User.Identity.IsAuthenticated)
                {
                    lblEstado.Text = $"Hola {User.Identity.GetUserName()}!";
                    phEstadoLogin.Visible = phCerrarSesion.Visible = true;
                    phFormularioLogin.Visible = false;
                }
                else
                {
                    phEstadoLogin.Visible = phCerrarSesion.Visible = false;
                    phFormularioLogin.Visible = true;
                }
            }
        }

        protected void btnIniciarSesion_Click(object sender, EventArgs e)
        {
            try
            {
                var userStore = new UserStore<IdentityUser>();
                var userManager = new UserManager<IdentityUser>(userStore);
                var user = userManager.Find(txtUsuario.Text, txtClave.Text);

                if (user != null)
                {
                    var adminAutenticacion = HttpContext.Current.GetOwinContext().Authentication;
                    var identidadUsuario = userManager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);

                    adminAutenticacion.SignIn(new AuthenticationProperties() { IsPersistent = false }, identidadUsuario);

                    Response.Redirect("~/Usuarios/WebFormLogin.aspx");
                }
                else
                {
                    lblInformacion.CssClass = "text-danger";
                    lblInformacion.Text = "Usuario o clave invalida";
                }
            }
            catch (Exception ex)
            {
                lblInformacion.CssClass = "text-danger";
                lblInformacion.Text = ex.Message;
            }
        }

        protected void btnCerrarSesion_Click(object sender, EventArgs e)
        {
            var administracionAutenticacion = HttpContext.Current.GetOwinContext().Authentication;
            administracionAutenticacion.SignOut();
            Response.Redirect("~/Usuarios/WebFormLogin.aspx");
        }
    }
}