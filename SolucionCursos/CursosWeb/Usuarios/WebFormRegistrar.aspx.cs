﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CursosWeb.Usuarios
{
    public partial class WebFormRegistrar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnRegistrar_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                var almacenamientoUsuarios = new UserStore<IdentityUser>();
                var administrar = new UserManager<IdentityUser>(almacenamientoUsuarios);
                var usuario = new IdentityUser { UserName = txtUsuario.Text };

                IdentityResult resultado = administrar.Create(usuario, txtClave.Text);

                if (resultado.Succeeded)
                {
                    var administracionAutenticacion = HttpContext.Current.GetOwinContext().Authentication;
                    var userIdentity = administrar.CreateIdentity(usuario, DefaultAuthenticationTypes.ApplicationCookie);
                    administracionAutenticacion.SignIn(new AuthenticationProperties() { IsPersistent = false }, userIdentity);

                    Response.Redirect("~/Usuarios/WebFormLogin.aspx");
                }
                else
                {
                    lblInformacion.CssClass = "text-danger";
                    lblInformacion.Text = resultado.Errors.FirstOrDefault();
                }
            }
        }
    }
}