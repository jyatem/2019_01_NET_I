﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Maestra.Master" AutoEventWireup="true" CodeBehind="WebFormRegistrar.aspx.cs" Inherits="CursosWeb.Usuarios.WebFormRegistrar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="mt-3">
        <div class="card">
            <div class="card-header text-white bg-dark">
                Registrar Usuario
            </div>
            <div class="card-body">
                <asp:ValidationSummary ID="vsResumen" runat="server" DisplayMode="BulletList" ShowMessageBox="false" ShowSummary="true" CssClass="alert alert-danger" />

                <!-- Usuario -->
                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="txtUsuario">Usuario:</label>
                    <div class="col-md-10">
                        <asp:TextBox ID="txtUsuario" runat="server" CssClass="form-control" Placeholder="Ingrese el usuario"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvUsuario" runat="server" ErrorMessage="El usuario es requerido" ControlToValidate="txtUsuario" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger" />
                        <asp:RegularExpressionValidator ID="revUsuario" runat="server" ErrorMessage="El usuario debe ser un correo" ControlToValidate="txtUsuario" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger" />
                    </div>
                </div>

                <!-- Clave -->
                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="txtClave">Clave:</label>
                    <div class="col-md-10">
                        <asp:TextBox ID="txtClave" runat="server" CssClass="form-control" Placeholder="Ingrese la clave" TextMode="Password"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvClave" runat="server" ErrorMessage="La clave es requerida" ControlToValidate="txtClave" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger" />
                    </div>
                </div>

                <!-- Confirmar Clave -->
                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="txtConfirmarClave">Confirmar Clave:</label>
                    <div class="col-md-10">
                        <asp:TextBox ID="txtConfirmarClave" runat="server" CssClass="form-control" Placeholder="Confirme la clave" TextMode="Password"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvConfirmarClave" runat="server" ErrorMessage="Confirme la clave" ControlToValidate="txtConfirmarClave" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger" />
                        <asp:CompareValidator ID="cvConfirmarClave" ErrorMessage="Las claves no coinciden" ControlToValidate="txtConfirmarClave" runat="server" ControlToCompare="txtClave" Display="Dynamic" SetFocusOnError="true" CssClass="alert-danger"/>
                    </div>
                </div>

                <!-- Botón de guardar -->
                <div class="form-group row">
                    <div class="offset-md-2 col-md-10">
                        <asp:Button ID="btnRegistrar" runat="server" Text="Registrar" CssClass="btn btn-dark" OnClick="btnRegistrar_Click" />
                    </div>
                </div>

                <!-- Label de información -->
                <div class="form-group row">
                    <div class="offset-md-2 col-md-10">
                        <asp:Label ID="lblInformacion" runat="server" Text="" CssClass="text-success"></asp:Label>
                    </div>
                </div>

            </div>
        </div>
    </div>

</asp:Content>
