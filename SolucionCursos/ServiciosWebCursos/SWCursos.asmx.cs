﻿using AccesoDatos.DTOs;
using LogicaNegocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace ServiciosWebCursos
{
    /// <summary>
    /// Summary description for SWCursos
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class SWCursos : System.Web.Services.WebService
    {
        //[WebMethod]
        public string HelloWorld()
        {
            return "Hola Jairo Yate!";
        }

        [WebMethod]
        public int Sumar(int i, int j)
        {
            return i + j;
        }

        [WebMethod]
        public List<CiudadDTO> RetornarCiudades()
        {
            FachadaCiudad fachadaCiudad = new FachadaCiudad();
            return fachadaCiudad.RetornarCiudadesDTO();
        }
    }
}
