﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriaCalculadora
{
    public class Calculadora
    {
        public int Sumar(int valor1, int valor2)
        {
            return valor1 + valor2;
        }

        public int Dividir(int numerador, int denominador)
        {
            if (denominador == 0)
                throw new DivideByZeroException();

            return numerador / denominador;
        }
    }
}
