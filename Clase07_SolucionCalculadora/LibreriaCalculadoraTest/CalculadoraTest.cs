﻿using System;
using LibreriaCalculadora;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LibreriaCalculadoraTest
{
    [TestClass]
    public class CalculadoraTest
    {
        [TestMethod]
        public void Sumar_2_3()
        {
            Calculadora calculadora = new Calculadora();

            int resultado = calculadora.Sumar(2, 3);

            Assert.AreEqual(5, resultado);
        }

        [TestMethod]
        public void Sumar_Neg1_Neg6()
        {
            Calculadora calculadora = new Calculadora();

            int resultado = calculadora.Sumar(-1, -6);

            Assert.AreEqual(-7, resultado);
        }

        [TestMethod]
        public void Dividir_8_4()
        {
            Calculadora calculadora = new Calculadora();

            int resultado = calculadora.Dividir(8, 4);

            Assert.AreEqual(2, resultado);
        }

        [TestMethod]
        [ExpectedException(typeof(DivideByZeroException))]
        public void DividirPorCero()
        {
            Calculadora calculadora = new Calculadora();

            int resultado = calculadora.Dividir(8, 0);
        }
    }
}
