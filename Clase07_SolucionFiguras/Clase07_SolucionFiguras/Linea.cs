﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clase07_SolucionFiguras
{
    public class Linea : Figura
    {
        public Linea(Color color) : base(color)
        {
        }

        public override void Pintar()
        {
            Console.WriteLine($"Pinta la línea de color {ColorFigura}");
        }
    }
}
