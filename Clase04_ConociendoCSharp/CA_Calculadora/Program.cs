﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Calculadora
{
    class Program
    {
        static void Main(string[] args)
        {
            int resultado = Calculadora.Sumar(14, 3);

            List<int> numeros = new List<int> { 2, 5, 13, 80, 121 };

            foreach (int numero in numeros)
            {
                Console.WriteLine("Numero {0} es {1}", numero, Calculadora.EsPar(numero) ? "par" : "no es par");
            }

            Console.ReadLine();
        }
    }
}
