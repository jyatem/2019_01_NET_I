﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Herencia
{
    public class Padre
    {
        // El guión bajo es para representar una variable global
        private int _informacionPrivada;

        protected string _informacionProtegida;

        public string Propiedad1Padre { get; set; }

        public int Propiedad2Padre { get; set; }

        public void MetodoPadre(int informacion)
        {
            _informacionPrivada = 10;
            Console.WriteLine($"Método del padre {informacion}");
        }
    }
}
