﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Clases
{
    public class Persona
    {
        public string Nombre { get; set; }

        public int Edad
        {
            get
            {
                int edad = DateTime.Today.AddTicks(-FechaNacimiento.Ticks).Year - 1;
                return edad;
            }
        }

        public DateTime FechaNacimiento { get; set; }

        public bool? Casado { get; set; }

        public Genero GeneroPersona { get; set; }

        public Persona()
        {
        }

        public Persona(string nombre, int edad)
        {
            Nombre = nombre;
            //Edad = edad;
            FechaNacimiento = DateTime.Now;
        }

        public Persona(int dia, string nombre, int mes, int anno)
        {
            FechaNacimiento = new DateTime(anno, mes, dia);
            
            Nombre = nombre;
        }

        public void MostrarInformacion()
        {
            Console.WriteLine($"Nombre: {Nombre}, Edad: {Edad}");
        }

        public void MostrarInformacionCasado()
        {
            Console.WriteLine("{0} tiene {1} años y {2}", Nombre, Edad, Casado.Value ? " esta casado" : "no esta casado");
        }

        public bool EsMayorEdad()
        {
            //if (Edad >= 18)
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}

            return Edad >= 18;
        }
    }
}
