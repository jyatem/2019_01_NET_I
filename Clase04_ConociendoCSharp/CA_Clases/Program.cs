﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Clases
{
    class Program
    {
        static void Main(string[] args)
        {
            #region [ Instanciando Persona con constructor sin parametros ]
            Persona persona1 = new Persona();
            persona1.Nombre = "Jairo Yate";
            //persona1.Edad = 38;
            persona1.Casado = true;
            persona1.GeneroPersona = Genero.Masculino;
            persona1.FechaNacimiento = new DateTime(1980, 8, 13);
            #endregion

            #region [ Instanciando Persona con llaves ]
            Persona persona2 = new Persona
            {
                Nombre = "Lina",
                //Edad = 39,
                FechaNacimiento = new DateTime(1979, 12, 29),
                Casado = true
            };
            #endregion

            #region [ Mostrar información de persona 1 - Concatenando ]
            Console.WriteLine("Nombre: " + persona1.Nombre + ", Edad: " + persona1.Edad + ", Fecha de Nacimiento: " + persona1.FechaNacimiento);

            Console.WriteLine("Nombre: {0}, Edad: {1}, Fecha de Nacimiento: {2}", persona2.Nombre, persona2.Edad, persona2.FechaNacimiento.ToShortDateString());

            Console.WriteLine($"Nombre: {persona1.Nombre}, Edad: {persona1.Edad}, Fecha de Nacimiento: {persona1.FechaNacimiento.ToString("yyyy-MMMM-dd")}");
            #endregion

            #region [ Instanciando con el constructor de 2 parametros ]
            Persona persona3 = new Persona("Mariana", 13);
            persona3.FechaNacimiento = new DateTime(2006, 2, 14);
            persona3.Casado = false;
            #endregion

            #region [ Instanciando con el constructor de 3 parametros ]
            Persona persona4 = new Persona(24, "Juan Manuel", 7, 2010);
            persona4.Casado = null;
            Console.WriteLine($"{persona4.Nombre} {persona4.FechaNacimiento}");

            if (persona4.Casado.HasValue)
            {
                Console.WriteLine("Si tiene valor en casado y es: " + persona4.Casado);
            }
            else
            {
                Console.WriteLine("Casado es: null");
            }

            #endregion

            #region [ Invocando métodos ]
            Console.WriteLine("**********************");
            persona1.MostrarInformacionCasado();
            persona3.MostrarInformacionCasado();

            if (persona1.EsMayorEdad())
            {
                Console.WriteLine($"{persona1.Nombre} es mayor de edad");
            }
            #endregion

            #region [ Generics ]
            List<int> numeroEnteros = new List<int>();

            numeroEnteros.Add(1);
            numeroEnteros.Add(15);
            numeroEnteros.Add(25);
            numeroEnteros.Add(10);
            numeroEnteros.Add(2);
            numeroEnteros.Add(10000000);
            numeroEnteros.Add(-5);

            foreach (var numero in numeroEnteros)
            {
                Console.WriteLine(numero);
            }

            List<string> listadoNombre = new List<string>();

            listadoNombre.Add("Jairo");
            listadoNombre.Add("Lina");
            listadoNombre.Add("Mariana");
            listadoNombre.Add("Juan");

            foreach (string nombre in listadoNombre)
            {
                Console.WriteLine(nombre);
            }

            List<string> otroListado = new List<string> { "Maria", "Jose", "Juan" };

            foreach (string nombre in otroListado)
            {
                Console.WriteLine(nombre);
            }

            Console.WriteLine("{0}{0}", Environment.NewLine);

            List<Persona> personas = new List<Persona>();
            personas.Add(persona1);
            personas.Add(persona2);
            personas.Add(persona3);
            personas.Add(persona4);

            personas.Add(new Persona { Nombre = "Raquel", Casado = true, FechaNacimiento = new DateTime(1969, 9, 14), GeneroPersona = Genero.Femenino });

            foreach (Persona persona in personas)
            {
                persona.MostrarInformacion();
            }
            #endregion

            Console.ReadLine();
        }
    }
}
