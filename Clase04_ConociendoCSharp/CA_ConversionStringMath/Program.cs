﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_ConversionStringMath
{
    class Program
    {
        static void Main(string[] args)
        {
            #region [ Convertir String a Número ]
            string strNumero = "123a";

            //int numero1 = int.Parse(strNumero);

            int numero1;
            bool esNumero = int.TryParse(strNumero, out numero1);

            if (esNumero)
            {
                Console.WriteLine(numero1);
            }
            else
            {
                Console.WriteLine($"{strNumero} no es un número válido");
            }

            //bool.TryParse
            //double.TryParse

            int numero2 = Convert.ToInt32("4567");

            string strObtenerValor = numero2.ToString();
            #endregion

            #region [ Funciones String ]
            string frase = "Frase de prueba";

            // Retorna la longitud de la cadena
            int longitud = frase.Length;

            // Si quiero saber si contiene una palabra
            bool siContiene = frase.Contains("se");

            // Volver minuscula
            string minuscula = frase.ToLower();

            // Volver mayuscula
            string mayuscula = frase.ToUpper();

            // Si quiero saber la posición del primer caracter
            int indice = frase.IndexOf('a');

            // Si quiero saber la última posición en la que aparece un caracter
            int ultimaPosicion = frase.LastIndexOf("ba");

            // Cortar parte de la cadena
            string corte = frase.Remove(5);

            // Reemplazar un caracter por otro
            string nueva = frase.Replace('a', '*');

            // Quitar espacios
            string sinEspacios = frase.Trim();

            // Quitar espacios al final
            frase.TrimEnd();

            // Quitar espacios al principio
            frase.TrimStart();

            //Obtener un pedazo de la cadena
            string trozo = frase.Substring(6, 2);

            String.Format("{0:d}", 12);

            #endregion

            #region [ Funciones Math ]
            // Redondeo hacia arriba
            double resultadoArriba = Math.Ceiling(5.4);

            // Redondeo hacia abajo
            double resultadoAbajo = Math.Floor(5.4);

            // Potencia
            double potencia = Math.Pow(2, 3);

            // Raíz cuadrada
            double raizCuadrada = Math.Sqrt(9);

            // Trunca valor
            double truncar = Math.Truncate(7.219);

            #endregion

            #region [ Ejercicio ]
            string ejercicio = "La controladora XYZWEWEADA no encontró la ruta '/paginaOtraY' o no ha sido implementada";

            string obtenerNombrePagina = ejercicio.Substring(ejercicio.IndexOf("/") + 1, ejercicio.LastIndexOf("'") - ejercicio.IndexOf("/") - 1);
            #endregion

            Console.ReadLine();
        }
    }
}
