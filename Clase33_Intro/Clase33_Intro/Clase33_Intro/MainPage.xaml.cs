﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Clase33_Intro
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        //private void BtnObtenerDatos_Clicked(object sender, EventArgs e)
        //{
        //    lblInformacion.Text = $"¡Hola {txtNombre.Text} {txtApellido.Text}!";
        //}

        private async void BtnObtenerDatos_Clicked(object sender, EventArgs e)
        {
            await DisplayAlert("Mensaje", $"¡Hola {txtNombre.Text} {txtApellido.Text}!", "Ok");
        }
    }
}
