﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Clase33_Intro
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PageTriqui : ContentPage
	{
        private Button[] _botones = new Button[9];

        private GestionTriqui _gestionTriqui = new GestionTriqui();

		public PageTriqui ()
		{
			InitializeComponent ();
            _botones[0] = btn1;
            _botones[1] = btn2;
            _botones[2] = btn3;
            _botones[3] = btn4;
            _botones[4] = btn5;
            _botones[5] = btn6;
            _botones[6] = btn7;
            _botones[7] = btn8;
            _botones[8] = btn9;
        }

        private void Btn_Clicked(object sender, EventArgs e)
        {
            _gestionTriqui.SetBoton(sender as Button);

            if (_gestionTriqui.ChequearGanador(_botones))
            {
                slGameOver.IsVisible = true;

                foreach (Button button in _botones)
                {
                    button.IsEnabled = false;
                }
            }
        }

        private void BtnJugarDeNuevo_Clicked(object sender, EventArgs e)
        {
            _gestionTriqui.ResetearJuego(_botones);
            slGameOver.IsVisible = false;

            foreach (Button button in _botones)
            {
                button.IsEnabled = true;
            }
        }
    }
}