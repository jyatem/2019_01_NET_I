﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Clase33_Intro
{
    public class GestionTriqui
    {
        private int _turnoJugador = 1;

        private int[,] _ganadores = new int[,]
        {
            { 0, 1, 2 },
            { 3, 4, 5 },
            { 6, 7, 8 },
            { 0, 3, 6 },
            { 1, 4, 7 },
            { 2, 5, 8 },
            { 0, 4, 8 },
            { 2, 4, 6 }
        };

        public bool ChequearGanador(Button[] botones)
        {
            bool gameOver = false;

            for (int i = 0; i < 8; i++)
            {
                int a = _ganadores[i, 0], b = _ganadores[i, 1], c = _ganadores[i, 2];

                Button b1 = botones[a], b2 = botones[b], b3 = botones[c];

                if (b1.Text == "" || b2.Text == "" || b3.Text == "")
                    continue;

                if (b1.Text == b2.Text && b2.Text == b3.Text)
                {
                    b1.BackgroundColor = b2.BackgroundColor = b3.BackgroundColor = Color.Aqua;
                    gameOver = true;
                    break;
                }
            }

            bool esEmpate = true;

            if (!gameOver)
            {
                foreach (Button b in botones)
                {
                    if (b.Text == "")
                    {
                        esEmpate = false;
                        break;
                    }
                }

                if (esEmpate)
                {
                    gameOver = true;
                }
            }

            return gameOver;
        }

        public void SetBoton(Button b)
        {
            if (b.Text == "")
            {
                b.Text = _turnoJugador == 1 ? "X" : "O";
                _turnoJugador = _turnoJugador == 1 ? 2 : 1;
            }
        }

        public void ResetearJuego(Button[] botones)
        {
            _turnoJugador = 1;

            foreach (Button button in botones)
            {
                button.Text = "";
                button.BackgroundColor = Color.Gray;
            }
        }
    }
}
