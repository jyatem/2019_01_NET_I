﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Clase33_Intro
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PageLayout : ContentPage
	{
		public PageLayout ()
		{
			InitializeComponent ();
		}
	}
}