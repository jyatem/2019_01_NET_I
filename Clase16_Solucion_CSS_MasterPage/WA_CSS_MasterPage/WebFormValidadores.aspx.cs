﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WA_CSS_MasterPage
{
    public partial class WebFormValidadores : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                lblMensaje.Text = $"{txtNombre.Text} {txtApellidos.Text}";
            }
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {

        }
    }
}