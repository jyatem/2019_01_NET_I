﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebFormEstilos.aspx.cs" Inherits="WA_CSS_MasterPage.WebFormEstilos" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <p>
            Parrafo sin estilo
        </p>
        <p style="color: #9a5ce2; text-align:right; background-color: #E4CAFF;">
            Parrafo con estilo
        </p>
    </form>
</body>
</html>
