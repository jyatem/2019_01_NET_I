﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBXam.Models
{
    public class TareaItem
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }

        public string Nombre { get; set; }

        public string Notas { get; set; }

        public bool Realizada { get; set; }
    }
}
