﻿using DBXam.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DBXam.Data
{
    public class TareaItemBaseDatos
    {
        readonly SQLiteAsyncConnection _contexto;

        public TareaItemBaseDatos(string dbRuta)
        {
            _contexto = new SQLiteAsyncConnection(dbRuta);
            _contexto.CreateTableAsync<TareaItem>().Wait();
        }

        public Task<List<TareaItem>> ObtenerTareasAsync()
        {
            return _contexto.Table<TareaItem>().ToListAsync();
        }

        public Task<TareaItem> ObtenerTareaAsync(int id)
        {
            return _contexto.Table<TareaItem>().Where(i => i.ID == id).FirstOrDefaultAsync();
        }

        public Task<int> GuardarTareaAsync(TareaItem tareaItem)
        {
            if (tareaItem.ID != 0)
            {
                return _contexto.UpdateAsync(tareaItem);
            }
            else
            {
                return _contexto.InsertAsync(tareaItem);
            }
        }

        public Task<int> EliminarTareaAsync(TareaItem tareaItem)
        {
            return _contexto.DeleteAsync(tareaItem);
        }

        public Task<List<TareaItem>> ObtenerTareasNoRealizadasAsync()
        {
            return _contexto.QueryAsync<TareaItem>("SELECT * FROM [TareaItem] WHERE [Realizada] = 0");
        }
    }
}
