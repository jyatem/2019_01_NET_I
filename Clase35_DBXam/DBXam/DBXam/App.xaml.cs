﻿using DBXam.Data;
using DBXam.Views;
using System;
using System.IO;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace DBXam
{
    public partial class App : Application
    {
        static TareaItemBaseDatos _tareaItemBaseDatos;

        public static TareaItemBaseDatos BaseDatos
        {
            get
            {
                if (_tareaItemBaseDatos == null)
                {
                    _tareaItemBaseDatos = new TareaItemBaseDatos(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "TareasSQLite.db3"));
                }

                return _tareaItemBaseDatos;
            }
        }

        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new TareasListPage());
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
