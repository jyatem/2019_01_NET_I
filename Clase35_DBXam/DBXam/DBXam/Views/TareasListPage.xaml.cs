﻿using DBXam.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DBXam.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TareasListPage : ContentPage
    {

        public TareasListPage()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            lstTareas.ItemsSource = await App.BaseDatos.ObtenerTareasAsync();
        }

        private async void ToolbarItem_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new TareaItemPage
            {
                BindingContext = new TareaItem()
            });
        }

        private async void LstTareas_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem != null)
            {
                await Navigation.PushAsync(new TareaItemPage
                {
                    BindingContext = e.SelectedItem as TareaItem
                });
            }
        }
    }
}