﻿using DBXam.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DBXam.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TareaItemPage : ContentPage
    {
        public TareaItemPage()
        {
            InitializeComponent();
        }

        private async void BtnGuardar_Clicked(object sender, EventArgs e)
        {
            var tareaItem = (TareaItem)BindingContext;
            await App.BaseDatos.GuardarTareaAsync(tareaItem);
            await Navigation.PopAsync();
        }

        private async void BtnEliminar_Clicked(object sender, EventArgs e)
        {
            var tareaItem = (TareaItem)BindingContext;
            await App.BaseDatos.EliminarTareaAsync(tareaItem);
            await Navigation.PopAsync();
        }

        private async void BtnCancelar_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }
    }
}