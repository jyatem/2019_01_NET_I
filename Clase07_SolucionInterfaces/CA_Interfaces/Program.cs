﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Interfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            MiClase miClase = new MiClase();
            miClase.MetodoDeLaInterfaz();

            IMiInterfaz desdeInterfaz = new MiClase();

            // IMiInterfaz miInterfaz = new IMiInterfaz(); No se puede instanciar una interfaz

            desdeInterfaz.MetodoDeLaInterfaz();

            // Invocar Duplicar Valor
            miClase.DuplicarValor(20);
            desdeInterfaz.DuplicarValor(5);

            Console.ReadLine();
        }
    }
}
