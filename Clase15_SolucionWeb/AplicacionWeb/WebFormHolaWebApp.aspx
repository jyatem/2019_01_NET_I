﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebFormHolaWebApp.aspx.cs" Inherits="AplicacionWeb.WebFormHolaWebApp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:TextBox ID="txtNombre" runat="server"></asp:TextBox>
            <br />
            <asp:Button ID="btnSaludar" runat="server" OnClick="btnSaludar_Click" Text="Botón Saludar" />
            <br />
            <asp:Label ID="lblResultado" runat="server"></asp:Label>
        </div>
    </form>
</body>
</html>
