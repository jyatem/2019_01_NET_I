﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebFormControles.aspx.cs" Inherits="AplicacionWeb.WebFormControles" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <table border="1">
            <tr>
                <th>Nombrel del control
                </th>
                <th>Control
                </th>
            </tr>
            <tr>
                <td>Calendario</td>
                <td>
                    <asp:Calendar ID="calFecha" runat="server" BackColor="White" BorderColor="#999999" CellPadding="4" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Height="180px" Width="200px">
                        <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
                        <NextPrevStyle VerticalAlign="Bottom" />
                        <OtherMonthDayStyle ForeColor="#808080" />
                        <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                        <SelectorStyle BackColor="#CCCCCC" />
                        <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
                        <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
                        <WeekendDayStyle BackColor="#66ccff" />
                    </asp:Calendar>
                </td>
            </tr>
            <tr>
                <td>Checkbox</td>
                <td>
                    <asp:CheckBox ID="chkTieneCarro" runat="server" Text="Tiene carro" Checked="true" /></td>
            </tr>
            <tr>
                <td>CheckBoxList</td>
                <td>
                    <asp:CheckBoxList ID="chkLenguajes" runat="server">
                        <asp:ListItem>C#</asp:ListItem>
                        <asp:ListItem>Html</asp:ListItem>
                        <asp:ListItem>Javascript</asp:ListItem>
                        <asp:ListItem>CSS</asp:ListItem>
                    </asp:CheckBoxList>
                </td>
            </tr>
            <tr>
                <td>DropDownList</td>
                <td>
                    <asp:DropDownList ID="ddlCiudad" runat="server">
                        <asp:ListItem>Medellin</asp:ListItem>
                        <asp:ListItem>Itagui</asp:ListItem>
                        <asp:ListItem>Envigado</asp:ListItem>
                        <asp:ListItem>Bello</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>Hidden Field</td>
                <td>
                    <asp:HiddenField ID="hdfValorOculto" runat="server" Value="100"/>
                </td>
            </tr>
            <tr>
                <td>Hyperlink</td>
                <td>
                    <asp:HyperLink ID="hlURL" runat="server" 
                        NavigateUrl="~/WebFormTabla.aspx" Target="_blank">Google</asp:HyperLink>
                </td>
            </tr>
            <tr>
                <td>Imagen</td>
                <td>
                    <asp:Image ID="imaEscudo" runat="server" ImageUrl="~/Imagenes/Imagen.jpg" Width="182px" Height="100px"/></td>
            </tr>
            <tr>
                <!-- Comentario Html -->
                <%--Comentario ASP.NET--%>
                <td>ListBox</td>
                <td>
                    <asp:ListBox ID="lstPasaTiempos" runat="server" Rows="3">
                        <asp:ListItem>xBox</asp:ListItem>
                        <asp:ListItem>Champion League</asp:ListItem>
                        <asp:ListItem>Cine</asp:ListItem>
                        <asp:ListItem>Libros</asp:ListItem>                        
                    </asp:ListBox></td>
            </tr>
            <tr>
                <td>Label</td>
                <td>
                    <asp:Label ID="lblInformacion" Text="Información" runat="server" />
                </td>
            </tr>
            <tr>
                <td>RadioButton</td>
                <td>
                    <asp:RadioButton Text="Verdadero" runat="server" ID="rdoVerdadero" Checked="true" GroupName="radio"/>
                    <asp:RadioButton Text="Falso" runat="server" ID="rdoFalso" GroupName="radio"/>
                </td>
            </tr>
            <tr>
                <td>RadioButtonList</td>
                <td>
                    <asp:RadioButtonList runat="server" ID="rdoLstOpciones">
                        <asp:ListItem Text="Opción 1" />
                        <asp:ListItem Text="Opción 2" Selected="True"/>
                        <asp:ListItem Text="Opción 3" />
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td>Textbox Multiple líneas</td>
                <td>
                    <asp:TextBox runat="server" ID="txtMultilinea" Height="50px" MaxLength="200" TextMode="MultiLine"/>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:Button Text="Obtener información" runat="server" ID="btnObtenerInformacion" OnClick="btnObtenerInformacion_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblValores" runat="server" />
                </td>
            </tr>
            </table>
    </form>
</body>
</html>
