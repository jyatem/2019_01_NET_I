﻿namespace WF_Contenedor
{
    partial class FormConUserControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.userControlPaisDepartamento = new WF_Contenedor.UserControlPaisDepartamento();
            this.btnSeleccionados = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // userControlPaisDepartamento
            // 
            this.userControlPaisDepartamento.Location = new System.Drawing.Point(48, 23);
            this.userControlPaisDepartamento.Name = "userControlPaisDepartamento";
            this.userControlPaisDepartamento.Size = new System.Drawing.Size(236, 110);
            this.userControlPaisDepartamento.TabIndex = 0;
            // 
            // btnSeleccionados
            // 
            this.btnSeleccionados.Location = new System.Drawing.Point(141, 139);
            this.btnSeleccionados.Name = "btnSeleccionados";
            this.btnSeleccionados.Size = new System.Drawing.Size(123, 23);
            this.btnSeleccionados.TabIndex = 1;
            this.btnSeleccionados.Text = "Seleccionados";
            this.btnSeleccionados.UseVisualStyleBackColor = true;
            this.btnSeleccionados.Click += new System.EventHandler(this.btnSeleccionados_Click);
            // 
            // FormConUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(332, 196);
            this.Controls.Add(this.btnSeleccionados);
            this.Controls.Add(this.userControlPaisDepartamento);
            this.Name = "FormConUserControl";
            this.Text = "FormConUserControl";
            this.ResumeLayout(false);

        }

        #endregion

        private UserControlPaisDepartamento userControlPaisDepartamento;
        private System.Windows.Forms.Button btnSeleccionados;
    }
}