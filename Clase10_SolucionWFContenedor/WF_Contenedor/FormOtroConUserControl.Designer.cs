﻿namespace WF_Contenedor
{
    partial class FormOtroConUserControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.userControlPaisDepartamento1 = new WF_Contenedor.UserControlPaisDepartamento();
            this.SuspendLayout();
            // 
            // userControlPaisDepartamento1
            // 
            this.userControlPaisDepartamento1.Location = new System.Drawing.Point(41, 35);
            this.userControlPaisDepartamento1.Name = "userControlPaisDepartamento1";
            this.userControlPaisDepartamento1.Size = new System.Drawing.Size(236, 113);
            this.userControlPaisDepartamento1.TabIndex = 0;
            // 
            // FormOtroConUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(305, 211);
            this.Controls.Add(this.userControlPaisDepartamento1);
            this.Name = "FormOtroConUserControl";
            this.Text = "FormOtroConUserControl";
            this.ResumeLayout(false);

        }

        #endregion

        private UserControlPaisDepartamento userControlPaisDepartamento1;
    }
}