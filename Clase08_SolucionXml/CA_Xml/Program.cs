﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CA_Xml
{
    class Program
    {
        static void Main(string[] args)
        {
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(@"C:\Diplomado\NET_I\Clase08_SolucionXml\CA_Xml\Empleados.xml");

            #region [ Por País ]
            XmlNodeList empleadosPorPais = xmlDocument.SelectNodes("/Empleados/Empleado[Pais='Ecuador']");

            foreach (XmlNode nodo in empleadosPorPais)
            {
                Console.WriteLine($"Nombre: {nodo["Nombre"].InnerText} de {nodo.Attributes["Ciudad"].Value}");
            }
            #endregion

            Console.WriteLine(Environment.NewLine);

            #region [ Por Ciudad ]
            XmlNodeList empleadosPorCiudad = xmlDocument.SelectNodes("/Empleados/Empleado[@Ciudad='Envigado']");

            foreach (XmlNode nodo in empleadosPorCiudad)
            {
                Console.WriteLine($"Nombre: {nodo["Nombre"].InnerText} de {nodo.Attributes["Ciudad"].Value}");
            }
            #endregion

            Console.WriteLine(Environment.NewLine);

            #region [ Por Genero ]
            XmlNodeList empleadosPorGenero = xmlDocument.SelectNodes("/Empleados/Empleado[@Genero='F']");

            foreach (XmlNode nodo in empleadosPorGenero)
            {
                Console.WriteLine($"Nombre: {nodo["Nombre"].InnerText} de {nodo.Attributes["Ciudad"].Value}");
            }
            #endregion

            #region [ Crear Empleado en el XML ]
            XmlElement empleado = xmlDocument.CreateElement("Empleado");

            // Atributos
            XmlAttribute id = xmlDocument.CreateAttribute("Id");
            id.Value = "6";

            XmlAttribute ciudad = xmlDocument.CreateAttribute("Ciudad");
            ciudad.Value = "Envigado";

            XmlAttribute genero = xmlDocument.CreateAttribute("Genero");
            genero.Value = "F";

            empleado.Attributes.Append(id);
            empleado.Attributes.Append(ciudad);
            empleado.Attributes.Append(genero);

            // Elementos de empleado
            XmlElement nombre = xmlDocument.CreateElement("Nombre");
            nombre.InnerText = "Maria";

            XmlElement pais = xmlDocument.CreateElement("Pais");
            pais.InnerText = "Colombia";

            empleado.AppendChild(nombre);
            empleado.AppendChild(pais);

            xmlDocument.DocumentElement.AppendChild(empleado);
            xmlDocument.Save(@"C:\Diplomado\NET_I\Clase08_SolucionXml\CA_Xml\EmpleadosConNuevo.xml");

            #endregion

            Console.ReadLine();
        }
    }
}
