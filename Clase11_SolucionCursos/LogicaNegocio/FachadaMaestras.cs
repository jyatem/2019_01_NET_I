﻿using AccesoDatos;
using Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicaNegocio
{
    public class FachadaMaestras
    {
        private DAODepartamento _daoDepartamento;
        private DAOCiudad _daoCiudad;
        private DAOCurso _daoCurso;
        
        public FachadaMaestras()
        {
            _daoDepartamento = new DAODepartamento();
            _daoCiudad = new DAOCiudad();
            _daoCurso = new DAOCurso();
        }

        #region [ Operaciones para Departamento ]
        public List<Departamento> RetornarDepartamentos()
        {
            return _daoDepartamento.RetornarDepartamentos();
        }
        #endregion

        #region [ Operaciones para Ciudad ]
        public List<Ciudad> RetornarCiudades()
        {
            return _daoCiudad.RetornarCiudades();
        }

        public void ActualizarCiudad(Ciudad ciudad)
        {
            // Validar información
            _daoCiudad.ActualizarCiudad(ciudad);
        }

        public void EliminarCiudad(int idCiudad)
        {
            if (_daoCiudad.EliminarCiudad(idCiudad) != 1)
            {
                throw new Exception("No se eliminó la ciudad");
            }
        }

        public Ciudad RetornarCiudad(int idCiudad)
        {
            Ciudad ciudad = _daoCiudad.RetornarCiudad(idCiudad);

            if (ciudad == null)
            {
                throw new Exception("La ciudad no existe");
            }

            return ciudad;
        }
        #endregion

        #region [ Operaciones para Curso ]
        public DataSet RetornarCursos()
        {
            return _daoCurso.RetornarCursos();
        }
        #endregion
    }
}
